package mappe.del1.hospital;

/**
 * Abstract class Person
 * contains general information all other classes that extends Person uses
 */
public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    public String getPersonnummer() {
        return socialSecurityNumber;
    }

    public void setPersonnummer(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullNavn(){
        return firstName + " " + lastName;
    }

    public Person(String firstName, String lastName, String socialSecurityNumber){
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }


    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", socialSecurityNumber='" + socialSecurityNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return getFullNavn().equalsIgnoreCase(person.getFullNavn());
    }
}
