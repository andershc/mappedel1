package mappe.del1.hospital.exception;

/**
 * RemoveException class
 * Used when trying to remove a person that doesn't exist in the register.
 */
public class RemoveException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * @param fullName Name of the Person that doesn't exist in the register.
     */
    public RemoveException(String fullName){
        super(fullName);
    }
}
