package mappe.del1.hospital;

/**
 * If a class implements Diagnosable it means that the object can be diagnosed
 */
public interface Diagnosable {
    void setDiagnosis(String diagnosis);
}
