package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.Objects;

public class Department {
    private String departmentName;
    private ArrayList<Employee> employees;
    private ArrayList<Patient> patients;

    /**
     * Creates an instance of Department with a list of employees and a list of patient
     * @param departmentName
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
        employees = new ArrayList<>();
        patients = new ArrayList<>();
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public ArrayList<Employee> getEmployees(){
        return employees;
    }

    /**
     * Adds an employee to the employee list if the person doesn't exist already
     * @param newEmployee
     */
    public void addEmployee(Employee newEmployee){
        if(!employees.contains(newEmployee)){
            employees.add(newEmployee);
        }
        else{
            throw new IllegalArgumentException(newEmployee.getFullNavn() + " is already registered");
        }
    }

    public ArrayList<Patient> getPatients(){
        return patients;
    }

    /**
     * Adds a petient to the patients list if the patient doesn't exist already
     * @param newPatient
     */
    public void addPatient(Patient newPatient){
        if(!patients.contains(newPatient)){
            patients.add(newPatient);
        }
        else{
            throw new IllegalArgumentException(newPatient.getFullNavn() + " is already registered");
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDepartmentName());
    }

    /**
     * removes a person from either employees or patients
     * throws a RemoveExcpeption if the person isn't found in any of the lists or if the input is null.
     * @param removePerson
     * @throws RemoveException
     */
    public void remove(Person removePerson) throws RemoveException{
        if(removePerson == null){
            throw new NullPointerException("Input cant be null");
        }
        else if(removePerson instanceof Employee) {
            if(employees.remove(removePerson) && removePerson.getFullNavn() != null){
            }
            else{
                throw new RemoveException(removePerson.getFullNavn() + " does not exist in the register");
            }

        }
        else if(removePerson instanceof Patient){
            if(patients.remove(removePerson) && removePerson.getFullNavn() != null){
            }
            else{
                throw new RemoveException(removePerson.getFullNavn() + " does not exist in the register");
            }
        }
        else{
            throw new RemoveException(removePerson.getFullNavn() + " does not exist in the register");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return getDepartmentName().equals(that.getDepartmentName());
    }

    @Override
    public String toString() {
        return "Department: " + departmentName + "\n";
    }
}
