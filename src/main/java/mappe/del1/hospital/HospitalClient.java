package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;

public class HospitalClient {
    public static void main(String[] args) throws RemoveException {
        Hospital hospital = new Hospital("St. Olavs Hospital");

        HospitalTestData.fillRegisterWithTestData(hospital); //Fills register with test data

        /**
         * Tries to remove an Employee that exists in the register
         */
        try {
            hospital.getDepartments().get(0).remove(new Employee("Odd Even", "Primtallet", ""));
        } catch(RemoveException | NullPointerException e){
            System.out.println(e.getMessage());
        }
        /**
         * Tries to remove a Patient that doesn't exist in the register
         */
        try{
            hospital.getDepartments().get(0).remove(new Patient("Oda", "Partallet", ""));
        } catch (RemoveException | NullPointerException e){
            System.out.println(e.getMessage());
        }
    }
}
