package mappe.del1.hospital;

/**
 * Patient class
 */
public class Patient extends Person implements Diagnosable {
    private String diagnosis = "";
    protected String getDiagnosis(){
        return diagnosis;
    }
    protected Patient(String firstName, String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Patient" +
                "diagnosis: " + diagnosis;
    }

    /**
     * A Patient can have a diagnosis
     * @param diagnosis
     */
    @Override
    public void setDiagnosis(String diagnosis){
        this.diagnosis = diagnosis;
    }
}
