package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Patient;
import mappe.del1.hospital.healthpersonal.doctor.Doctor;

/**
 * Surgeon class
 */
public class Surgeon extends Doctor {
    public Surgeon(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Surgeon can set a diagnosis for a patient
     * @param patient
     * @param diagnosis
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis){
        patient.setDiagnosis(diagnosis);
    }
}
