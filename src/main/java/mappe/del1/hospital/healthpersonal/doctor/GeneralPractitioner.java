package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Patient;
import mappe.del1.hospital.healthpersonal.doctor.Doctor;

/**
 * GeneralPractitioner class that is a type of doctor
 */
public class GeneralPractitioner extends Doctor {
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * GeneralPractitioner can set a diagnosis for a patient
     * @param patient
     * @param diagnosis
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis){
        patient.setDiagnosis(diagnosis);
    }
}
