package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Employee;
import mappe.del1.hospital.Patient;

public abstract class Doctor extends Employee {
    protected Doctor(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Doctors can set a diagnosis for a Patient.
     * @param patient
     * @param diagnosis
     */
    abstract public void setDiagnosis(Patient patient, String diagnosis);
}
