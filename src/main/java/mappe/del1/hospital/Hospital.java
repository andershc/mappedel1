package mappe.del1.hospital;

import java.util.ArrayList;

/**
 * Hospital class
 */
public class Hospital {
    private final String hospitalName;
    private ArrayList<Department> departments;

    /**
     * Creates an instance of Hospital with a list of the departments
     * @param hospitalName
     */
    public Hospital(String hospitalName){
        this.hospitalName = hospitalName;
        departments = new ArrayList<>();
    }

    public String getHospitalName(){
        return hospitalName;
    }
    public ArrayList<Department> getDepartments(){
        return departments;
    }

    /**
     * Adds a department if it doesn't already exist
     * @param department
     */
    public void addDepartment(Department department){
        if(!departments.contains(department)){
            departments.add(department);
        }
        else{
            throw new IllegalArgumentException("The derpartment is already registered");
        }
    }

    @Override
    public String toString() {
        return "Hospital: " + hospitalName + "\n";
    }
}
