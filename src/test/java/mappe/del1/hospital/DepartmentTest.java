package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class DepartmentTest {
    /**
     * RemoveTest contains tests for remove(). Tests for null input, and if a Patient/Employee
     * does not appear in the register. If any of those are true then an exception is thrown.
     */
    @Nested
    class RemoveTest {
        @Test
        public void nullTest(){
            Department emergency = new Department("Akutten");
            assertThrows(NullPointerException.class, () -> emergency.remove(null));
        }

        @Test
        public void removeEmployeePositive() {
            Department emergency = new Department("Akutten");
            Employee inco = new Employee("Inco", "Gnito", "");
            emergency.addEmployee(inco);

            Assertions.assertTrue(emergency.getEmployees().contains(inco));
            try {
                emergency.remove(inco);
            } catch (RemoveException | NullPointerException exception) {
                assertNull(exception);
            }
            assertFalse(emergency.getEmployees().contains(inco));
        }

        @Test
        public void removePatientPositive() {
            Department emergency = new Department("Akutten");
            Patient inco = new Patient("Inco", "Gnito", "");
            emergency.addPatient(inco);

            Assertions.assertTrue(emergency.getPatients().contains(inco));
            try {
                emergency.remove(inco);
            } catch (RemoveException | NullPointerException exception) {
                assertNull(exception);
            }
            assertFalse(emergency.getPatients().contains(inco));
        }


        @Test
        public void removeNegative() {
            Department emergency = new Department("Akutten");
            Employee erna = new Employee("Odd Even", "Primtallet", "");
            assertThrows(RemoveException.class, () -> emergency.remove(erna));
        }
    }
}